(function($) {
    "use strict";

    //===== Prealoder
    $(window).on('load', function(event) {
        $('.proloader').delay(500).fadeOut(500);
    });


    /*----------------------------------------
                       Wow js
        ------------------------------------------*/

    $("div.lazy").lazyload({
        effect: "fadeIn"
    });

    // sticky
    var wind = $(window);
    var sticky = $('.header-bar-area');
    wind.on('scroll', function() {
        var scroll = wind.scrollTop();
        if (scroll < 100) {
            sticky.removeClass('sticky');
        } else {
            sticky.addClass('sticky');
        }
    });


    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $('.site-nav-wrap li a').each(function() {
        if (this.href === path) {
            $(this).addClass('active');
        }
    });

    /* -------------------------------------
                Responsive menu
        -------------------------------------- */
    var siteMenuClone = function() {

        $('.js-clone-nav').each(function() {
            var $this = $(this);
            $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
        });

        setTimeout(function() {

            var counter = 0;
            $('.site-mobile-menu .has-children').each(function() {
                var $this = $(this);

                $this.prepend('<span class="arrow-collapse collapsed">');

                $this.find('.arrow-collapse').attr({
                    'data-toggle': 'collapse',
                    'data-target': '#collapseItem' + counter,
                });

                $this.find('> ul').attr({
                    'class': 'collapse',
                    'id': 'collapseItem' + counter,
                });

                counter++;

            });

        }, 1000);

        $('body').on('click', '.js-menu-toggle', function(e) {
            var $this = $(this);
            e.preventDefault();

            if ($('body').hasClass('offcanvas-menu')) {
                $('body').removeClass('offcanvas-menu');
                $this.removeClass('active');
            } else {
                $('body').addClass('offcanvas-menu');
                $this.addClass('active');
            }
        })

    };
    siteMenuClone();

    $('.lang_list').on('click', function() {
        $('.dropdown_lng').toggleClass('show');
        $('.lang_list').toggleClass('maf');

    });

    /*-------------------------------------
                Magnific Popup js
        --------------------------------------*/


    $('.project_gal').magnificPopup({
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
    });



    // $(".slider-box").owlCarousel({
    //     items: 1,
    //     nav: true,
    //     navText: ['<i class="ion-ios-arrow-back"></i>', '<i class="ion-ios-arrow-forward"></i>'],
    //     dot: false,
    //     margin:10,
    //     autoplayTimeout: 2000,
    //     loop: true,
    //     autoplay: false,
    //     smartSpeed: 450,
    //     responsiveClass: true,
    //     responsive: {
    //         0: {
    //             items: 1,
    //             margin:5
    //         },
    //         600: {
    //             items: 1,
    //             margin:5
    //         }
    //     }
    // });

    $(".slider-box").owlCarousel({
    items: 1,
    nav: true,
    navText: ['<i class="ion-ios-arrow-back"></i>', '<i class="ion-ios-arrow-forward"></i>'],
    dot: false,
    autoplayTimeout: 2000,
    loop: true,
    margin: 10,
    autoplay: false,
    smartSpeed: 1000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            margin:4,

        },
        600: {
            items: 1,

        },
        1000: {
            items: 1,

        }
    }
});
      $(".slider_box").owlCarousel({
    items: 1,
    nav: true,
    navText: ['<i class="ion-ios-arrow-forward"></i>', '<i class="ion-ios-arrow-back"></i>'],
    dot: false,
    rtl:true,
    autoplayTimeout: 2000,
    loop: true,
    margin: 10,
    autoplay: false,
    smartSpeed: 1000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            margin:4,

        },
        600: {
            items: 1,

        },
        1000: {
            items: 1,

        }
    }
});

    //Product Showcase slider
    var slider_one = new Swiper('.product_showcase', {
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        speed: 1000,
        // autoplay: {
        //     delay: 2500,
        //     disableOnInteraction: false,
        // },
        navigation: {
            nextEl: '.showcase_prev',
            prevEl: '.showcase_next',
        },
        breakpoints: {

            575: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            767: {
                slidesPerView: 1,
                spaceBetween: 0
            }

        }
    });

    //Partner slider
    var slider_two = new Swiper('.partner-wrap', {
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true,
        speed: 1000,
        // autoplay: {
        //     delay: 2500,
        //     disableOnInteraction: false,
        // },
        navigation: {
            nextEl: '.partner_prev',
            prevEl: '.partner_next',
        },
        breakpoints: {

            575: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 30
            }

        }
    });
    // Gallery slider
    var slider_three = new Swiper('.gallery-slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        speed: 1000,
        // autoplay: {
        //     delay: 2500,
        //     disableOnInteraction: false,
        // },
        navigation: {
            nextEl: '.gallery_prev',
            prevEl: '.gallery_next',
        },
        breakpoints: {

            575: {
                slidesPerView: 1,
                spaceBetween: 20
            }

        }

    });
    // Gallery slider
    var slider_three = new Swiper('.gallery_slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        speed: 1000,
        // autoplay: {
        //     delay: 2500,
        //     disableOnInteraction: false,
        // },
        navigation: {
            nextEl: '.gallery_prev',
            prevEl: '.gallery_next',
        },
        breakpoints: {

            575: {
                slidesPerView: 1,
                spaceBetween: 40
            }

        }
    });

    //===== Back to top

    // Show or hide the sticky footer button
    $(window).on('scroll', function(event) {
        if ($(this).scrollTop() > 600) {
            $('.back-to-top').fadeIn(200)
        } else {
            $('.back-to-top').fadeOut(200)
        }
    });


    //Animate the scroll to yop
    $('.back-to-top').on('click', function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0,
        }, 1500);
    });






    // Hamburger-menu
    // $('.hamburger-menu').on('click', function() {
    //     $('.hamburger-menu .line-top, .ofcavas-menu').toggleClass('current');
    //     $('.hamburger-menu .line-center').toggleClass('current');
    //     $('.hamburger-menu .line-bottom').toggleClass('current');
    // });


    // NiceSelect

    // $(document).ready(function() {
    //     $('select').niceSelect();
    // });

    /*=========================
      magnificPopup video-view
    ===========================*/
    $('.venobox').venobox();






})(jQuery);